﻿#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);


    const int N = 5;
    int array[N][N] = { {0,1,2,3,4}, {5,6,7,8,9} };
    int i, j, sum = 0;
    for (i = 0; i < N; ++i)
    {

        for (j = 0; j < N; ++j)
        {
            array[i][j] = i + j;
            std::cout << array[i][j];
            if (buf.tm_mday % N == i)
            {
                sum += array[i][j];


            }

        }

        std::cout << "\n";



    }
    std::cout << sum << std::endl;

}